# Basic set up for jQuery/Bootstrap project with npm tools
Follow these steps to start working.

### Step 1: Get the Source
To get it from git, run from cmd/terminal
```git clone [path]```
Or download the zip file and extract it.

### Step 2: Install
We need [Node](https://nodejs.org/) - install node first if you don't have.
In the cmd/terminal, go to the project folder (where we have the package.json) and run
```npm install```

### Step 3: For development, run start and check the update in the browser
```gulp start``` will bundle the files and start the server (localhost:3000 by default)
If we make changes in the css or js files, it will be reflected in the browser

### To create minified packages
```gulp bundle-app-min``` will create the minified bundled files

## Package contains:

 * Browserify
 * BrowserSync
 * Less
 * Bootstrap
 * jQuery
 * and scripts for the js and css packaging

### How it works
TBD
